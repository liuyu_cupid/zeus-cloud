package com.zeus.reponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ZeusResponse<T> {

    private Integer code;

    private String message;

    private T data;

    public ZeusResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public static <T> ZeusResponse<T> success(T data) {
        return new ZeusResponse<>(200, "success", data);
    }

    public static <T> ZeusResponse<T> fail(T data) {
        return new ZeusResponse<>(500, "fail", data);
    }


}
