package com.zeus.cupid.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.zeus.cupid.service.CupidService;
import com.zeus.entity.Ring;
import com.zeus.reponse.ZeusResponse;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
@RefreshScope
@RequestMapping("/cupid")
@DefaultProperties(defaultFallback = "globalFallbackMethod")
public class CupidController {

    @Resource
    private CupidService cupidService;

    @Value("${zeus.cloud.author}")
    private String author;

    @Value("${zeus.cloud.profile}")
    private String profile;

    @GetMapping("/love")
    public ZeusResponse<String> getLove() {
        return ZeusResponse.success("love will come ! O(∩_∩)O哈哈~");
    }

    @GetMapping("/propose")
    public ZeusResponse<String> propose() {
        ZeusResponse<List<Ring>> listZeusResponse = cupidService.ringList();
        List<Ring> data = listZeusResponse.getData();
        if (CollectionUtils.isNotEmpty(data)) {
            Optional<Ring> ringOptional = data.stream().findAny();
            return ZeusResponse.success("Send  " + ringOptional.get().getName() + " to propose your girlfriend");
        }
        return ZeusResponse.fail("not find any ring");
    }

    @GetMapping("/author")
    public ZeusResponse<String> author() {
        return ZeusResponse.success(author);
    }

    public ZeusResponse<String> globalFallbackMethod() {
        return ZeusResponse.success("服务器出错了！请稍后重试");
    }


}
