package com.zeus.cupid.service;

import com.zeus.entity.Ring;
import com.zeus.reponse.ZeusResponse;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Component
public class HystrixCupidService implements CupidService {

    @Override
    public ZeusResponse<List<Ring>> ringList() {
        Ring ring = new Ring("02", 0.00, "狗尾巴草");
        return ZeusResponse.success(Collections.singletonList(ring));
    }


}
