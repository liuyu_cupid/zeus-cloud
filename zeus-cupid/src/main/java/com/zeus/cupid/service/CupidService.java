package com.zeus.cupid.service;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.zeus.entity.Ring;
import com.zeus.reponse.ZeusResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "zeus-hermes", fallback = HystrixCupidService.class)
public interface CupidService {

    @GetMapping("/hermes/ring-list")
    ZeusResponse<List<Ring>> ringList();

}
