package com.zeus.hermes.controller;

import com.zeus.entity.Ring;
import com.zeus.reponse.ZeusResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/hermes")
public class RingController {

    @GetMapping("/ring-list")
    public ZeusResponse<List<Ring>> ringList() {
//        int a = 10 /0;
        return ZeusResponse.success(Collections.singletonList(new Ring("00000", 20000, "永恒之约")));
    }

}
